import json

from voluptuous import Schema, Required, Any, Optional
from tap_csv.logger import LOGGER as logger
from tap_csv.backend import selector

CONFIG_CONTRACT = Schema({
    Required('start_date'): str,
    Required('bucket'): str,
    Required('backend'): Any('s3', 'file'),
    Required('tables'): [{
        Required('name'): str,
        Required('pattern'): str,
        Required('key_properties'): [str],
        Required('format'): Any('csv', 'excel'),
        Optional('search_prefix'): str,
        Optional('unzip'): Any('true', 'false'),
        Optional('encoding'): str,
        Optional('dialect'): str,
        Optional('delimiter'): str,
        Optional('doublequote'): Any('true', 'false'),
        Optional('escapechar'): str,
        Optional('fieldnames'): str,
        Optional('field_names'): [str],
        Optional('quotechar'): str,
        Optional('quoting'): Any('QUOTE_MINIMAL', 'QUOTE_ALL', 'QUOTE_NONNUMERIC', 'QUOTE_NONE'),
        Optional('skipinitialspace'): Any('true', 'false'),
        Optional('strict'): Any('true', 'false'),
        Optional('worksheet_name'): str,
        Optional('schema_overrides'): {
            str: {
                Required('type'): Any(str, [str]),
                Required('_conversion_type'): Any('string',
                                                  'integer',
                                                  'number',
                                                  'date-time')
            }
        }
    }],
    Optional('aws_access_key_id'): str,
    Optional('aws_secret_access_key'): str
})


def load(filename):
    config = {}

    try:
        with open(filename) as handle:
            config = json.load(handle)
    except:
        logger.fatal("Failed to decode config file. Is it valid json?")
        raise RuntimeError

    CONFIG_CONTRACT(config)

    backend = selector[config['backend']]
    if not all(hasattr(config, field)
               for field in backend.list_of_required_config_fields):
        logger.fatal(
            """Failed to required fields specific to backend in config file.
            Did you provide the AWS key and secret?""")
        raise RuntimeError

    return config
