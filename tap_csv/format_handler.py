# pylint: disable=R1705
# ^^^^ There is just no pleasing it in get_row_iterator
import tap_csv.csv_handler
import tap_csv.excel_handler
from tap_csv.logger import LOGGER as logger

# # Moved into s3.py and file.py
# def get_file_handle(config, s3_path):


def get_row_iterator(config, table_spec, s3_path):
    backend = config['backend']

    if table_spec['format'] == 'csv':
        raw_stream = backend.get_raw_stream(config, s3_path)
        return tap_csv.csv_handler.get_row_iterator(
            table_spec, raw_stream)

    elif table_spec['format'] == 'excel':
        file_handle = backend.get_file_handle(config, s3_path)
        return tap_csv.excel_handler.get_row_iterator(
            table_spec, file_handle)

    else:  # pylint
        logger.fatal("Impossible file format in get_row_iterator")
        return None
