import re
import os
from pathlib import Path
from datetime import timezone, datetime as dt
from tap_csv.logger import LOGGER as logger


# #These can be re-used from s3.py:
# def sample_file(config, table_spec, s3_path, sample_rate, max_records):
# def sample_files(config, table_spec, s3_files,
#                  sample_rate=10, max_records=1000, max_files=5):


def list_files_in_bucket(config, bucket, search_prefix=None):
    #pylint: disable=unused-argument
    # "config" is there for compatibility with S3, you could eves pass None

    # https://boto3.amazonaws.com/v1/documentation/api/latest/\
    # reference/core/session.html#boto3.session.Session.client
    # bucket really means parent directory in this context

    s3_objects = []

    top_path = Path(bucket).resolve()
    logger.debug('Walking directory contents of "{}".'
                 .format(top_path))
    for root, _dirs, files in os.walk(top_path, topdown=True):
        for name in files:
            absolute_path = Path(root) / name
            relative_path = absolute_path.relative_to(top_path).__fspath__()
            if (not search_prefix) or relative_path.startswith(search_prefix):

                attribs = absolute_path.stat()
                last_modified = dt.fromtimestamp(attribs.st_mtime,
                                                 tz=timezone.utc)
                # example format: dateutil.parser.parse('2017-05-01T00:00:00Z')
                # AWS seems to use UTC: https://stackoverflow.com/a/55061182
                # The file system definitely uses UTC
                obj = {'Key': relative_path,
                       'LastModified': last_modified}
                s3_objects.append(obj)

    logger.info("Found {} files.".format(len(s3_objects)))

    return s3_objects


def get_input_files_for_table(config, table_spec, modified_since=None):
    bucket = config['bucket']
    to_return = []
    pattern = table_spec['pattern']
    matcher = re.compile(pattern)

    logger.debug(
        'Checking bucket "{}" for keys matching "{}"'
        .format(bucket, pattern))

    s3_objects = list_files_in_bucket(
        config, bucket, table_spec.get('search_prefix'))

    for s3_object in s3_objects:
        key = s3_object['Key']
        last_modified = s3_object['LastModified']

        logger.debug('Last modified: {}'.format(last_modified))

        if(matcher.search(key) and
           (modified_since is None or modified_since < last_modified)):
            logger.debug('Will download key "{}"'.format(key))
            to_return.append({'key': key, 'last_modified': last_modified})
        else:
            logger.debug('Will not download key "{}"'.format(key))

    to_return = sorted(to_return, key=lambda item: item['last_modified'])

    return to_return


# moved here from format_handler.py because it's specific to the storage engine
def get_file_handle(config, s3_path):
    top_path = Path(config['bucket']).resolve()
    path_to_file = top_path / s3_path

    file_handle = open(path_to_file, 'rb')
    # rb to get bytes, because the generator-maker transcodes bytes to utf8/...
    return file_handle


get_raw_stream = get_file_handle
