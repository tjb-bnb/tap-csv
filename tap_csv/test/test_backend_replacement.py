# pylint: disable=redefined-outer-name
# ^^^ pylint complains about pytest fixtures
# https://stackoverflow.com/questions/46089480/pytest-fixtures-redefining-name-from-outer-scope-pylint
import argparse
import dateutil
import pytest
import tap_csv.config
import tap_csv.backend
from tap_csv.__init__ import do_sync


@pytest.fixture
def args():  # no state, dunno how to tell pytest
    dico = {'config': 'tap_csv/test/config.json', 'state': None}
    args = argparse.Namespace(**dico)
    return args


@pytest.fixture
def config():
    return {'start_date': dateutil.parser.parse('2017-05-01T00:00:00Z'),
            'bucket': 'tap_csv/test/csv',
            'backend': tap_csv.backend.backend_file, 'tables': [
                {'name': 'table_name', 'pattern': '(.*)\\.csv$',
                 'key_properties': ['id'], 'format': 'csv',
                 'encoding': 'utf8', 'delimiter': ','}]}


@pytest.fixture
def table_spec(config):
    return config['tables'][0]


@pytest.fixture
def table_name(table_spec):
    return table_spec['name']


@pytest.fixture
def modified_since(config):
    return config['start_date']


@pytest.fixture
def backend():
    return tap_csv.backend.backend_file


def test_main_parser(args):
    assert args.config
    do_sync(args)
    # "assert no exception RuntimeError"


def test_get_input_files_for_table(config, table_spec,
                                   modified_since, backend):
    s3_files = backend.get_input_files_for_table(
        config, table_spec, modified_since)
    assert s3_files and len(s3_files) > 0


def test_list_files_in_bucket(config):
    bucket = config['bucket']
    search_prefix = None
    s3_objects = tap_csv.file.list_files_in_bucket(config, bucket, search_prefix)
    found_files = set(s['Key'] for s in s3_objects)
    assert {'sample.csv', 'subdir/sample.csv'}.issubset(found_files)


def test_load_missing_aws_secrets():
    with pytest.raises(RuntimeError):
        tap_csv.config.load('tap_csv/test/config_no_secret.json')


def test_sample_files(config, backend, table_spec):
    s3_files = backend.get_input_files_for_table(config, table_spec)
    assert s3_files
    samples = backend.sample_files(config, table_spec, s3_files)
    assert samples


def test_get_file_handle(config, backend):
    s3_path = 'sample.csv'
    file_handle = backend.get_file_handle(config, s3_path)
    line = file_handle.readline()
    assert line == b'id,First Name, Last Name\n'
