from dataclasses import dataclass
import typing
#from tap_csv.s3 import sample_file, sample_files,
#    get_input_files_for_table, list_files_in_bucket
import tap_csv.s3 as s3
import tap_csv.file as file

@dataclass
class Backend:
    """Selector to plug in file system or s3 buckets access code"""
    sample_file: typing.Callable
    sample_files: typing.Callable
    get_input_files_for_table: typing.Callable
    list_files_in_bucket: typing.Callable
    get_file_handle: typing.Callable
    get_raw_stream: typing.Callable
    list_of_required_config_fields: list

backend_s3 = Backend(sample_file=s3.sample_file,
                     sample_files=s3.sample_files,
                     get_input_files_for_table=s3.get_input_files_for_table,
                     list_files_in_bucket=s3.list_files_in_bucket,
                     get_file_handle=s3.get_file_handle,
                     get_raw_stream=s3.get_raw_stream,
                     list_of_required_config_fields=[
                         'aws_access_key_id', 'aws_secret_access_key'
                     ])

backend_file = Backend(sample_file=s3.sample_file,
                       sample_files=s3.sample_files,
                       get_input_files_for_table=file.get_input_files_for_table,
                       list_files_in_bucket=file.list_files_in_bucket,
                       get_file_handle=file.get_file_handle,
                       get_raw_stream=file.get_raw_stream,
                       list_of_required_config_fields=[])

selector = {'s3': backend_s3,
            'file': backend_file
            }
