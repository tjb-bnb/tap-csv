import codecs
import csv
import re
import gzip
from tap_csv.logger import LOGGER as logger


def generator_wrapper(reader):
    to_return = {}

    for row in reader:
        # likely bugs:
        # https://github.com/robertjmoore/tap-csv/compare/master...aroder:master
        # row[0] = row[0].replace(u'\ufeff', '') if encoding == 'utf-16-le'
        # row[0] = row[0].replace(quotechar, '') if quotechar
        for key, value in row.items():
            if key is None:
                key = '_s3_extra'

            formatted_key = key

            # remove non-word, non-whitespace characters
            formatted_key = re.sub(r"[^\w\s]", '', formatted_key)

            # replace whitespace with underscores
            formatted_key = re.sub(r"\s+", '_', formatted_key)

            to_return[formatted_key.lower()] = value

        yield to_return


def get_row_iterator(table_spec, raw_stream):
    # I'm sorry these get_row_iterator has a different signature than
    # the caller in format_handler.py

    # we use a protected member of the s3 object, _raw_stream, here to create
    # a generator for data from the s3 file.
    file_zipped = codecs.iterdecode(raw_stream,
                                    encoding=table_spec.get('encoding', 'utf-8'))

    # https://github.com/fishtown-analytics/tap-s3-csv/compare/\
    # master...Adovenmuehle:master
    if table_spec.get('unzip') == 'true':
        file_stream = gzip.GzipFile(fileobj=file_zipped)
    else:
        file_stream = file_zipped

    try:
        dictreader_args = {}
        if 'field_names' in table_spec:
            dictreader_args['fieldnames'] = table_spec['field_names']
            # backwards compatibility:recognize both field_names and fieldnames
        conv_string = lambda input: input
        conv_boole = lambda input: (input == 'true')
        conv_quoting = lambda input: getattr(csv, input)
        # https://github.com/fishtown-analytics/tap-s3-csv/compare/master...Adovenmuehle:master
        possible_args = {'dialect': conv_string, 'fieldnames': conv_string,
                         'delimiter': conv_string, 'doublequote': conv_boole,
                         'escapechar': conv_string, 'quotechar': conv_string,
                         'quoting': conv_quoting,
                         'skipinitialspace': conv_boole, 'strict': conv_boole
                         }
        for key in possible_args:
            if key in table_spec:
                dictreader_args[key] = possible_args[key](table_spec[key])
    except(ValueError, KeyError):
        logger.fatal("Cannot cast types of DictReader arguments in config file.")
        raise RuntimeError

    reader = csv.DictReader(file_stream, **dictreader_args)

    return generator_wrapper(reader)
