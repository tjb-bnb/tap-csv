#!/usr/bin/env python

from setuptools import setup

setup(name='tap-csv',
      version='0.0.2',
      description='Singer.io tap for extracting CSV files from S3',
      author='Fishtown Analytics',
      url='http://fishtownanalytics.com',
      classifiers=['Programming Language :: Python :: 3 :: Only'],
      py_modules=['tap_csv'],
      install_requires=[
          'boto3==1.4.4',
          'singer-python==1.5.0',
          'voluptuous==0.10.5',
          'xlrd==1.0.0',
      ],
      entry_points='''
          [console_scripts]
          tap-csv=tap_csv:main
      ''',
      packages=['tap_csv'])
