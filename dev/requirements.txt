boto3==1.4.4
pylint==2.5.3
pytest==5.4.3
singer-python==1.5.0
voluptuous==0.10.5
xlrd==1.0.0
