# tap-csv
Authors: Tilman Bohl, Connor McArthur (connor@fishtownanalytics.com)

[Singer](singer.io) tap that produces JSON-formatted data following
the [Singer spec](https://github.com/singer-io/getting-started/blob/master/SPEC.md). Based on tap-s3-csv by Connor McArthur.

Given a configuration that specifies a directory, a file pattern to match, a file format (`csv` or `excel`),
and a directory name, this tap reads new files from the file system or an Amazon
s3 bucket, parses them, infers a schema, and outputs the data according to
the Singer spec. It supports custom encodings, delimiters, and anything csv.DictReader in Python can do.

Beware: it is based on an ancient Singer version which does not support --discovery. Further instructions are located at https://github.com/singer-io/getting-started, see also https://meltano.com/tutorials/csv-with-postgres.html

### Installation

To run locally, clone this repo, then run:

```bash
python setup.py install
```

Now you can run:

```
tap-csv --config configuration.json
```

to generate data.

### How it works

This tap:

 - Searches the directory for files matching the spec given.
 - Samples 1000 records out of the first five files found to infer datatypes.
 - Iterates through files from least recently modified to most recently modified, outputting data according
   to the generated schema & Singer spec.
 - After completing each file, it writes out state that can be used to enable incremental replication.

### Example

Given a source file: `./csv-bucket/csv-exports/today.csv`

```csv
id,First Name, Last Name
1,Michael,Bluth
2,Lindsay,Bluth Fünke
3,Tobias,Fünke
```

And a config file:

```json
{
    "start_date": "2017-05-01T00:00:00Z",
    "bucket": "csv-bucket",
    "backend": "file",
    "tables": [
        {
            "name": "bluths",
            "pattern": "csv-exports/(.*)\\.csv$",
            "key_properties": ["id"],
            "format": "csv",
            "encoding": "utf8",
            "delimiter": ","
        }
    ]
}
```

An output record might look like:

```json
{
  "id": 3,
  "first_name": "Tobias",
  "last_name": "Funke",
  "_s3_source_bucket": "csv-bucket",
  "_s3_source_file": "csv-exports/today.csv",
  "_s3_source_lineno": 4,
  "_s3_extra": null
}
```

### Input File Gotchas

- Input files MUST have a header row.
- Input files MUST have cells fully populated. Missing cells will break the integration. Empty cells
  are handled by the tap.
- If you have the choice, use CSV, not Excel. This tap is able to stream CSV files from S3 row-by-row,
  but it cannot stream Excel files. CSV files are more efficient and more reliable.
- This tap can convert datetimes, but it does not infer date-time as an output datatype. If you want
  the tap to convert a field to datetime, you must specify `date-time` as the `_conversion_type` in
  `schema_overrides`. See "Configuration Format" below for more information.


### Configuration Format

See below for an exhaustive list of configuration fields:

```javascript
{
    // The storage backend. "file" is file system, "s3" is Amazon S3.
    "backend": "file",

    // AWS credentials for s3 backend. Ignored by file backend.
    "aws_access_key_id": "",
    "aws_secret_access_key": "",

    // the start date to use on the first run. the tap outputs an updated state on each
    // run which you can use going forward for incremental replication
    "start_date": "2017-05-01T00:00:00Z",

    // file backend: the top-level directory
    // It can be a symlink, but we do not follow symbolic links within it.
    // s3 backend: the bucket
    "bucket": "/directory",

    // table definitions. you can specify multiple tables to be pulled from a given
    // bucket.
    "tables": [
        // example csv table definition with schema overrides
        {
            // table name to output
            "name": "bluths_from_csv",

            // you can limit (remotely) the paths searched in s3 if there are
            // many files in your bucket
            // the file backend simply checks if the file name relative to bucket
            // starts with this prefix.
            "search_prefix": "csv-exports",

            // pattern to match (locally) in the bucket
            "pattern": "csv-exports/(.*)\\.csv$",

            // if true, unzip the file before reading it as a csv. Default false.
            // ignored for excel.
            "unzip": false,

            // Input encoding. Output is always utf8.
            "encoding": "utf8",

            // CSV: any option Python's csv.DictReader knows
            // default delimiter: ","
            "delimiter": ","
            "dialect": "excel"
            "doublequote": "True"
            "escapechar": "None"
            "quotechar": "\""
            "quoting": "QUOTE_MINIMAL"
            "skipinitialspace": "False"
            "strict": "False"

            // primary key for this table. if append only, use:
            //   ["_s3_source_bucket", "_s3_source_file", "_s3_source_lineno"]
            "key_properties": ["id"],

            // format, either "csv" or "excel"
            "format": "csv",

            // if the files don't have a header row, you can specify the field names
            "field_names": ["id", "first_name", "last_name"],

            // for any field in the table, you can hardcode the json schema datatype.
            // "_conversion_type" is the type that the tap will try to coerce the field
            // to -- one of "string", "integer", "number", or "date-time". this tap
            // also assumes that all fields are nullable to be more resilient to empty cells.
            "schema_overrides": {
                "id": {
                    "type": ["null", "integer"],
                    "_conversion_type": "integer"
                },

                // if you want the tap to enforce that a field is not nullable, you can do
                // it like so:
                "first_name": {
                    "type": "string",
                    "_conversion_type": "string"
                }
            }
        },

        // example excel definition
        {
            "name": "bluths_from_excel",
            "pattern": "excel-exports/(.*)\\.xlsx$",
            "key_properties": ["id"],
            "format": "excel",

            // the excel definition is identical to csv except that you must specify
            // the worksheet name to pull from in your xls(x) file.
            "worksheet_name": "Names"
        }
    ]
}
```

### Output Format

- Column names have whitespace removed and replaced with underscores.
- They are also downcased.
- A few extra fields are added for help with auditing:
  - `_s3_source_bucket`: The bucket that this record came from
  - `_s3_source_file`: The path to the file that this record came from
  - `_s3_source_lineno`: The line number in the source file that this record was found on
  - `_s3_extra`: If you specify field names in the config, and there are more records in a row than field names, the overflow will end up here.

  ### Singer Gotchas

  Singer's documentation is not that easy unless you are already coming from Stitch:
  - Overview, see Stitch docu: https://www.stitchdata.com/docs
  - Singer Tutorial: adapt from target-postgres https://github.com/datamill-co/target-postgres and from https://meltano.com/tutorials/csv-with-postgres.html
  - Singer technical documentation: https://github.com/singer-io/getting-started

  You cannot feed the state file from the last run right into the next. Instead:

  ```shell
  if [ -s state-history.json ] ; then
    tail -n1 state-history.json >state.json

    (cd tap-fishtown-csv;
    ../venv/tap-csv/bin/tap-csv --config tap_csv_config.json --state ../state.json)|

    (cd target-postgres;
    ../venv/target-postgres/bin/target-postgres --config target_postgres_config.json

    ) >> state-history.json

  else
    rm -rf state.json state-history.json
    (cd tap-fishtown-csv;
    ../venv/tap-fishtown-csv/bin/tap-csv --config tap_fishtown_csv_config.json) |
    (cd target-postgres;
    ../venv/target-postgres/bin/target-postgres --config target_postgres_config.json
    ) >> state-history.json
  fi
  ```

### Resonable future improvements

Use smart_open to access lots of backends: https://github.com/fishtown-analytics/tap-s3-csv/compare/master...retentionscience:master
Dependencies upgrade: https://github.com/robertjmoore/tap-csv/compare/master...aroder:master
